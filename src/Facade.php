<?php

namespace Kreango\CulqiPanel;

use Culqi\Culqi;
use Illuminate\Support\Facades\Facade as BaseFacade;

/**
 * @method static string getMode()
 * @method static string getToken()
 * @method static Culqi getInstance()
 */
class Facade extends BaseFacade
{
    public static function getFacadeAccessor()
    {
        return Culqi::class;
    }
}