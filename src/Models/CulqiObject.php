<?php

namespace Kreango\CulqiPanel\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CulqiObject extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'id',
        'object',
        'type',
        'data',
        'state',
    ];

    protected $casts = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];
}